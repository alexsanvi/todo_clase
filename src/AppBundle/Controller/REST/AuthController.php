<?php

namespace AppBundle\Controller\REST;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends BaseApiController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response('', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @Route("/auth/google")
     */
    public function googleLoginAction(Request $request)
    {
        $json = json_decode($request->getContent(), true);

        $googleJwt = json_decode(file_get_contents(
            "https://www.googleapis.com/plus/v1/people/me?access_token=" . $json['access_token']));

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(
            array('email'=>$googleJwt->emails[0]->value));

        if ($user)
        {
            $jwtManager = $this->get('lexik_jwt_authentication.jwt_manager');

            return new JsonResponse(['token' => $jwtManager->create($user)]);
        }
        else
            return $this->respuestaUsuarioNoAutorizado();
    }
}
