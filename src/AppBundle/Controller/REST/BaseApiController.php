<?php


namespace AppBundle\Controller\REST;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JMS\Serializer\SerializationContext;

class BaseApiController extends Controller
{
    private function setContent($data, $response, $arrGruposSerializacion)
    {

        if (!is_null($data))
        {
            $result['data'] = $data;

            $datos_serializados = $this->get('jms_serializer')->serialize(
                $result,
                "json",
                SerializationContext::create()->setGroups($arrGruposSerializacion));

            $response->setContent($datos_serializados);
        }
    }

    protected function respuestaCorrecta($data, $statusCode = Response::HTTP_OK, $arrGruposSerializacion=array('Default'))
    {
        $response = new Response();

        $this->setContent($data, $response, $arrGruposSerializacion);

        $response->setStatusCode($statusCode);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    protected function respuestaNoEncontrado()
    {
        return $this->respuestaCorrecta(null, Response::HTTP_NOT_FOUND);
    }

    protected function respuestaUsuarioNoAutorizado()
    {
        return $this->respuestaCorrecta(null, Response::HTTP_UNAUTHORIZED);
    }

    protected function respuestaDatosIncorrectos($data, $arrGruposSerializacion=array('Default'))
    {
        return $this->respuestaCorrecta($data, Response::HTTP_BAD_REQUEST, $arrGruposSerializacion);
    }

    protected function guardaValidando($entity, $arrGruposSerializacion=array('Default'))
    {
        $validator = $this->get('validator');
        $errors = $validator->validate($entity);

        if (count($errors) > 0)
        {
            $errorsMessages = array();
            foreach($errors as $error)
                $errorsMessages[] = $error->getMessage();

            return $this->respuestaDatosIncorrectos($errorsMessages, $arrGruposSerializacion);
        }
        else
        {
            $em = $this->getDoctrine()->getManager();

            $em->persist($entity);
            $em->flush();

            return $this->respuestaCorrecta($entity, Response::HTTP_CREATED, $arrGruposSerializacion);
        }
    }

    protected function deleteEntity($id, $entityName)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($entityName)->find($id);

        if (is_null($entity) === true)
            return $this->respuestaNoEncontrado();

        $em->remove($entity);
        $em->flush();

        return $this->respuestaCorrecta(null, Response::HTTP_NO_CONTENT);
    }

    protected function getEntity($id, $entityName, $arrGruposSerializacion=array('Default'))
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository($entityName)->find($id);
        if (is_null($entity) === true)
            return $this->respuestaNoEncontrado();

        return $this->respuestaCorrecta($entity, Response::HTTP_OK, $arrGruposSerializacion);
    }

    protected function postEntity(
        Request $request, $entityName,
        callable $funcionConfiguracionEntidad=null,
        $arrGruposSerializacion=array('Default'))
    {
        $entity = $this->get('jms_serializer')->deserialize($request->getContent(), $entityName, "json");

        if (!is_null($funcionConfiguracionEntidad))
            $funcionConfiguracionEntidad($entity);

        return $this->guardaValidando($entity, $arrGruposSerializacion);
    }

    protected function updateEntity(Request $request, $id, $entityName,
                                    callable $funcionConfiguracionEntidad=null,
                                    $arrGruposSerializacion=array('Default'))
    {
        try
        {
            $data = json_decode($request->getContent(), true);
            $data['id'] = $id;
            $entity = $this->get('jms_serializer')->deserialize(json_encode($data), $entityName, "json");

            if (!is_null($funcionConfiguracionEntidad))
                $funcionConfiguracionEntidad($entity);

            return $this->guardaValidando($entity, $arrGruposSerializacion);
        }
        catch(\Exception $exception)
        {
            return $this->respuestaCorrecta(null, Response::HTTP_NOT_FOUND);
        }
    }
}