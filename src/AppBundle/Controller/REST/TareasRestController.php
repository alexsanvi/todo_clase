<?php

namespace AppBundle\Controller\REST;

use AppBundle\Entity\Tarea;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class TareasRestController extends BaseApiController
{
    /**
     * @Route("/tareas", name="get_tareas")
     * @Route("/tareas/ordenadas/{order}", name="get_tareas_ordenadas")
     * @Method("GET")
     * @ApiDoc(
     *  section="Tareas",
     *  resource=true,
     *  description="Devuelve una colección de tareas. Los parámetros se pasan por query string.",
     *  requirements={
     *      {
     *          "name"="orden",
     *          "dataType"="string",
     *          "description"="nombre del campo por el que se desea ordenar (nombre|estado|prioridad|fechaAlta)"
     *      }
     *  },
     *  filters={
     *      {"name"="nombre", "dataType"="string", "description"="busca porciones de nombres, no es necesario introducir el nombre completo"},
     *      {"name"="prioridad", "dataType"="integer", "description"="Va de 1 a 3"},
     *      {"name"="estado", "dataType"="string", "pattern"="(TERMINADA|PENDIENTE)"}
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function cgetAction(Request $request, $order='prioridad')
    {
        $em = $this->getDoctrine()->getManager();

        $estado = $request->query->get('estado');
        $prioridad = $request->query->get('prioridad');
        $nombre = $request->query->get('nombre');

        $tareas = $em->getRepository(Tarea::class)->getTareasFiltradas(
            $estado, $prioridad, $nombre, $order);

        return $this->respuestaCorrecta($tareas);
    }

    /**
     * @Route("/tareas/{id}", name="get_tarea", requirements={"id": "\d+"})
     * @Method("GET")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Muestra una tarea",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a mostrar"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function getAction($id)
    {
        return $this->getEntity($id, Tarea::class);
    }

    /**
     * @Route("/tareas", name="post_tareas")
     * @Method("POST")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Crea una nueva tarea. Los parámetros se pasan por JSon.",
     *  parameters={
     *      {
     *          "name"="nombre",
     *          "dataType"="string",
     *          "required"=true,
     *          "description"="Nombre de la tarea. Debe ser único."
     *      },
     *      {
     *          "name"="prioridad",
     *          "dataType"="integer",
     *          "required"=false,
     *          "description"="Prioridad de la tarea de 1 a 3. Si no se indica se le asigna el valor 1"
     *      },
     *      {
     *          "name"="estado",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Estado de la tarea (TERMINADA|PENDIENTE). Si no se indica se le asigna el valor PENDIENTE"
     *      },
     *      {
     *          "name"="fechaAlta",
     *          "dataType"="datetime",
     *          "required"=false,
     *          "description"="Fecha de alta de la tarea. Si no se indica tomará la fecha actual"
     *      }
     *  },
     *  statusCodes={
     *         201="Tarea creada",
     *         400="Datos no válidos",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function postAction(Request $request)
    {
        $funcionConfiguracionTarea = function(Tarea $tarea) use ($request)
        {
            $usuario = $this->get('security.token_storage')->getToken()->getUser();

            $tarea->setUsuario($usuario);
        };

        return $this->postEntity(
            $request, Tarea::class, $funcionConfiguracionTarea);
    }

    /**
     * @Route("/tareas/{id}", name="delete_tarea", requirements={"id": "\d+"})
     * @Method("DELETE")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Elimina una tarea. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a eliminar"
     *      }
     *  },
     *  statusCodes={
     *         204="Resultado OK",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function deleteAction($id)
    {
        return $this->deleteEntity($id, Tarea::class);
    }

    /**
     * @Route("/tareas/{id}", name="update_tarea", requirements={"id": "\d+"})
     * @Method("PUT")
     * @ApiDoc(
     *  section="Tareas",
     *  description="Modifica una tarea. Los parámetros se pasan por Json.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id de la tarea a modificar"
     *      }
     *  },
     *  parameters={
     *      {
     *          "name"="nombre",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Nombre de la tarea. Debe ser único."
     *      },
     *      {
     *          "name"="prioridad",
     *          "dataType"="integer",
     *          "required"=false,
     *          "description"="Prioridad de la tarea de 1 a 3. Si no se indica se le asigna el valor 1"
     *      },
     *      {
     *          "name"="estado",
     *          "dataType"="string",
     *          "required"=false,
     *          "description"="Estado de la tarea (TERMINADA|PENDIENTE). Si no se indica se le asigna el valor PENDIENTE"
     *      },
     *      {
     *          "name"="fechaAlta",
     *          "dataType"="datetime",
     *          "required"=false,
     *          "description"="Fecha de alta de la tarea. Si no se indica tomará la fecha actual"
     *      }
     *  },
     *  statusCodes={
     *         200="Resultado OK",
     *         400="Datos no válidos",
     *         404="Tarea no encontrada",
     *         401="Usuario no autorizado"
     *  },
     *  headers={
     *      {
     *          "required"=true,
     *          "name"="Authorization",
     *          "description"="Bearer {token}"
     *      }
     *  }
     * )
     */
    public function updateAction(Request $request, $id)
    {
        return $this->updateEntity($request, $id, Tarea::class);
    }

    /**
     * @Route("/tareas/usuario", name="get_tareas_usuario_actual")
     * @Method("GET")
     */
    public function tareasUsuarioAction(Request $request)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        if ($usuario)
            return $this->respuestaCorrecta($usuario->getTareas());

        return $this->respuestaUsuarioNoAutorizado();
    }
}
