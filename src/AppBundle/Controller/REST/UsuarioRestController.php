<?php

namespace AppBundle\Controller\REST;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class UsuarioRestController extends BaseApiController
{
    private function encriptaPassword(User $usuario)
    {
        if ($usuario->getPassword() !== null && $usuario->getPassword() !== '')
        {
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($usuario, $usuario->getPassword());

            $usuario->setPassword($encoded);
        }
    }

    /**
     * @Route("/usuarios", name="get_usuarios")
     * @Method("GET")
     */
    public function cgetAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository(User::class)->findAll();

        return $this->respuestaCorrecta($usuarios, Response::HTTP_OK, array('usuario'));
    }

    /**
     * @Route("/usuarios/{id}", name="get_usuario")
     * @Method("GET")
     */
    public function getAction($id)
    {
        return $this->getEntity($id, User::class, array('usuario'));
    }

    /**
     * @Route("/auth/register")
     * @Method("POST")
     */
    public function postAction(Request $request)
    {
        $funcionConfiguracionUsuario = function(User $usuario) use ($request)
        {
            $this->encriptaPassword($usuario);

            $usuario->setAvatar(
                $request->getUriForPath(
                    $this->getParameter('url_avatars_directory') . 'default.png'));
        };

        return $this->postEntity(
            $request, User::class,
            $funcionConfiguracionUsuario, array("usuario"));
    }

    /**
     * @Route("/profile/password")
     * @Method("PUT")
     */
    public function putPasswordAction(Request $request)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        $data = json_decode($request->getContent(), true);

        $usuario->setPassword($data['password']);
        $this->encriptaPassword($usuario);

        return $this->guardaValidando($usuario, array('usuario'));
    }


    /**
     * @Route("/usuarios/{id}", name="delete_usuario")
     * @Method("DELETE")
     */
    public function deleteAction($id)
    {
        return $this->deleteEntity($id, User::class);
    }

    /**
     * @Route("/profile/avatar")
     * @Method("PUT")
     */
    public function updateAvatarAction(Request $request)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        if ($usuario)
        {
            $data = json_decode($request->getContent(), true);

            if ($data['avatar'] !== null)
            {
                $arr_avatar = explode(',', $data['avatar']);
                $avatar = base64_decode($arr_avatar[1]);

                if ($avatar)
                {
                    $fileName = $usuario->getUsername().'-'.time().'.jpg';
                    $filePath = $this->getParameter('avatars_directory').$fileName;
                    $urlAvatar = $request->getUriForPath($this->getParameter('url_avatars_directory').$fileName);

                    $usuario->setAvatar($urlAvatar);

                    $ifp = fopen($filePath, "wb");
                    if ($ifp)
                    {
                        $ok = fwrite($ifp, $avatar);
                        if ($ok)
                            return $this->guardaValidando($usuario, array('usuario'));

                        fclose($ifp);
                    }
                }
            }

            $errores[] = "No se ha podido cargar la imagen del avatar";
            return $this->respuestaDatosIncorrectos($errores);
        }
        else
            return $this->respuestaNoEncontrado();
    }

    /**
     * @Route("/profile")
     * @Method("GET")
     */
    public function profileAction(Request $request)
    {
        $usuario = $this->get('security.token_storage')->getToken()->getUser();

        if ($usuario)
            return $this->respuestaCorrecta($usuario, Response::HTTP_OK, array('usuario'));

        return $this->respuestaUsuarioNoAutorizado();
    }
}
