<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Tarea
 *
 * @ORM\Table(name="tarea")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TareaRepository")
 * @UniqueEntity("nombre", message="Ya existe una tarea con el nombre {{ value }}")
 */
class Tarea
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     * @Assert\NotBlank(message="El nombre no se puede quedar vacío")
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="prioridad", type="integer")
     * @Assert\NotBlank(message="La prioridad no se puede quedar vacía")
     * @Assert\Range(
     *     min = 1,
     *     max = 3,
     *     minMessage = "La prioridad no puede ser menor que {{ limit }}",
     *     maxMessage = "La prioridad no puede ser mayor que {{ limit }}",
     *     invalidMessage = "La prioridad debe estar entre 1 y 3"
     * )
     */
    private $prioridad;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255)
     * @Assert\NotBlank(message="El estado no se puede quedar vacío")
     * @Assert\Choice(choices = {"PENDIENTE", "TERMINADA"}, message = "El estado debe ser 'PENDIENTE' o 'TERMINADA'")
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaAlta", type="datetime")
     * @JMS\Type("DateTime<'d/m/Y H:i'>")
     * @JMS\SerializedName("fecha")
     * @Assert\NotBlank(message="La fecha de alta no se puede quedar vacía")
     */
    private $fechaAlta;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="tareas")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @JMS\Accessor(getter="getIdUsuario")
     * @JMS\SerializedName("idUser")
     * @JMS\Type("integer")
     */
    private $usuario;

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("codigo")
     */
    public function getCodigo()
    {
        return $this->getId() . " - " . $this->getNombre();
    }

    public function getIdUsuario()
    {
        return $this->usuario->getId();
    }

    public function __construct()
    {
        $this->estado = "PENDIENTE";
        $this->prioridad = 1;
        $this->fechaAlta = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Tarea
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set prioridad
     *
     * @param integer $prioridad
     *
     * @return Tarea
     */
    public function setPrioridad($prioridad)
    {
        $this->prioridad = $prioridad;

        return $this;
    }

    /**
     * Get prioridad
     *
     * @return int
     */
    public function getPrioridad()
    {
        return $this->prioridad;
    }

    /**
     * Set estado
     *
     * @param string $estado
     *
     * @return Tarea
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaAlta
     *
     * @param \DateTime $fechaAlta
     *
     * @return Tarea
     */
    public function setFechaAlta($fechaAlta)
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * Get fechaAlta
     *
     * @return \DateTime
     */
    public function getFechaAlta()
    {
        return $this->fechaAlta;
    }

    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

}

