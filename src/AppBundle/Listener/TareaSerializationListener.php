<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Tarea;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;

class TareaSerializationListener implements EventSubscriberInterface
{
    private $securityTokenStorage;

    public function setSecurityTokenStorage($securityTokenStorage)
    {
        $this->securityTokenStorage = $securityTokenStorage;
    }

    static public function getSubscribedEvents()
    {
        return array(
            array(
                'event' => 'serializer.post_serialize',
                'class' => 'AppBundle\Entity\Tarea',
                'method' => 'onPostSerialize'),
        );
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        $tarea = $event->getObject();
        if ($tarea instanceof Tarea)
        {
            $usuario = $this->securityTokenStorage->getToken()->getUser();

            if ($usuario->getId() === $tarea->getUsuario()->getId())
                $event->getVisitor()->addData('owned', true);
            else
                $event->getVisitor()->addData('owned', false);
        }
    }
}