<?php

namespace tests\AppBundle\Controller\REST;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TareasRestControllerTest extends WebTestCase
{
    public function testGet()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/tareas');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $resultTest['data'] = array();
        $this->assertJsonStringEqualsJsonString(
            $client->getResponse()->getContent(),
            json_encode($resultTest));
    }

    public function testPost()
    {
        $client = static::createClient();

        $datos = array('nombre'=>'Terminar el API REST');

        $postData = array(
                'nombre' => 'Terminar el API REST'
        );

        $client->request(
             'POST',
             '/api/v1/tareas',
            $postData,
             array(),
             array('CONTENT_TYPE' => 'application/json')
        );

        $this->assertEquals(201, $client->getResponse()->getStatusCode());

        $resultTest = json_decode($client->getResponse()->getContent(), true);
        $this->assertContains(
            $resultTest['data']['nombre'],
            'Terminar el API REST');
    }
}